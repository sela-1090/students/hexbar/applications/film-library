import pytest
from flask import Flask
from databases import Database as db

@pytest.fixture
def app():
    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    db.init_app(app)
    
    with app.app_context():
        db.create_all()

    yield app

    with app.app_context():
        db.session.remove()
        db.drop_all()

@pytest.fixture
def client(app):
    return app.test_client()

def test_get_movies(app, client):
    with app.app_context():
        film1 = Film(1, "The Great Film", "John Producer", "Jane Director", ["Best Picture"], 2023, 9.5, 100, "Drama")
        film2 = Film(2, "Action Adventure", "Alex Producer", "Adam Director", ["Action Hero"], 2022, 8.8, 75, "Action")
        film3 = Film(3, "Comedy Central", "Chris Producer", "Carla Director", ["Best Comedy"], 2021, 7.9, 120, "Comedy")
        db.session.add(film1)
        db.session.add(film2)
        db.session.add(film3)
        db.session.commit()

    response = client.get('/movies')
    assert response.status_code == 200

    data = response.json
    assert len(data) == 3
    assert data[0]['f_name'] == 'The Great Film'
    assert data[1]['f_name'] == 'Action Adventure'
    assert data[2]['f_name'] == 'Comedy Central'
